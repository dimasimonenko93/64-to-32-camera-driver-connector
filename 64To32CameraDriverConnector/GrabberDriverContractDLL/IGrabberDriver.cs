﻿// <copyright file="IGrabberDriver.cs" company="Delphi Software LTD">
// Copyright (c) Delphi Software LTD 2019. All rights reserved.
// </copyright>

using System.IO;
using System.ServiceModel;

namespace GrabberDriverContracts
{
    /// <summary>
    /// Represents the WCF contract interface.
    /// </summary>
    [ServiceContract]
    public interface IGrabberDriver
    {
        /// <summary>
        /// Gets the image from camera with 64 bit DLL,
        /// and return it as a stream to the cleant.
        /// </summary>
        /// <returns>The stream.</returns>
        [OperationContract]
        Stream Grab();

        /// <summary>
        /// Exit the Main method of the process,
        /// so the process will be closed.
        /// </summary>
        [OperationContract(IsOneWay = true)]
        void Stop();
    }
}
