﻿// <copyright file="Program.cs" company="Delphi Software LTD">
// Copyright (c) Delphi Software LTD 2019. All rights reserved.
// </copyright>

using System;
using System.Drawing;

namespace Grabber32bitClient
{
    /// <summary>
    /// The entry point class of the grabber client.
    /// </summary>
    internal class Program
    {
        private static void Main(string[] args)
        {
            Grabber64bitClient grabber64BitClient = new Grabber64bitClient();

            Console.ReadKey();

            grabber64BitClient.Start();
            Console.WriteLine("grabber64BitClient has been started.");

            using (Image image = grabber64BitClient.Grab())
            {
                image.Save(@"D:\Visual Studio Projects\64-to-32-camera-driver-connector\64To32CameraDriverConnector\Grabber32bitClient\bin\Debug\image.png");
            }

            Console.ReadKey();
            grabber64BitClient.Stop();
        }
    }
}
