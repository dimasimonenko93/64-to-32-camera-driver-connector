﻿// <copyright file="Grabber64bitClient.cs" company="Delphi Software LTD">
// Copyright (c) Delphi Software LTD 2019. All rights reserved.
// </copyright>

using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.ServiceModel;
using GrabberDriverContracts;

namespace Grabber32bitClient
{
    /// <summary>
    /// The class that represent WCF client for connection to the WCF Grabber64bitService.
    /// </summary>
    public class Grabber64bitClient
    {
        private ChannelFactory<IGrabberDriver> channelFactory;

        /// <summary>
        /// Gets WCF channel that using for calls WCF Grabber64bitService methods.
        /// </summary>
        public IGrabberDriver WcfChannel { get; private set; }

        /// <summary>
        /// Creates and starts new WCF service process
        /// and connect to it.
        /// </summary>
        public void Start()
        {
            ProcessStartInfo procInfo = new ProcessStartInfo();

            procInfo.FileName = @"D:\Visual Studio Projects\64-to-32-camera-driver-connector\64To32CameraDriverConnector\Grabber64bitService\bin\Debug\Grabber64bitService.exe";

            // Process without window
            // procInfo.RedirectStandardOutput = true;
            // procInfo.UseShellExecute = false;
            procInfo.CreateNoWindow = true;

            // Process.Start(procInfo);

            string address = "net.pipe://localhost/Grabber64bitService/GrabberDriver";
            NetNamedPipeBinding binding = new NetNamedPipeBinding(NetNamedPipeSecurityMode.None);
            EndpointAddress endpoint = new EndpointAddress(address);

            this.channelFactory = new ChannelFactory<IGrabberDriver>(binding, endpoint);

            this.WcfChannel = this.channelFactory.CreateChannel();
        }

        /// <summary>
        /// Gets stream from WCF service and return image from it.
        /// </summary>
        /// <returns>The image.</returns>
        public Image Grab()
        {
            Image image;

            using (Stream stream = this.WcfChannel.Grab())
            {
                image = Image.FromStream(stream);
            }

            return image;
        }

        /// <summary>
        /// Calls WCF service Stop() method and
        /// causes a communication object to transition from its current state into the closed state.
        /// </summary>
        public void Stop()
        {
            this.WcfChannel.Stop();
            this.channelFactory.Close();
        }
    }
}
