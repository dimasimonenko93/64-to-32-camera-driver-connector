﻿// <copyright file="GrabberDriver.cs" company="Delphi Software LTD">
// Copyright (c) Delphi Software LTD 2019. All rights reserved.
// </copyright>

using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.ServiceModel;
using System.Threading;
using GrabberDriverContracts;

namespace Grabber64bitService
{
    /// <summary>
    /// The class that represent WCF service.
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class GrabberDriver : IGrabberDriver
    {
        private readonly ManualResetEvent closeMainProcess;

        private readonly Image image = Image.FromFile(@"D:\Visual Studio Projects\64-to-32-camera-driver-connector\64To32CameraDriverConnector\Grabber64bitService\bin\Debug\image.png");
        private readonly ImageFormat imageFormat = ImageFormat.Png;

        /// <summary>
        /// Initializes a new instance of the <see cref="GrabberDriver"/> class.
        /// </summary>
        /// <param name="closeMainProcess">An event that signals that the main process should be closed.</param>
        public GrabberDriver(ManualResetEvent closeMainProcess)
        {
            this.closeMainProcess = closeMainProcess;
        }

        /// <inheritdoc/>
        public Stream Grab()
        {
            var stream = new MemoryStream();
            this.image.Save(stream, this.imageFormat);
            stream.Position = 0;
            return stream;
        }

        /// <inheritdoc/>
        public void Stop()
        {
            this.closeMainProcess.Set();
        }
    }
}
