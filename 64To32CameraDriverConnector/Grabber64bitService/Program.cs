﻿// <copyright file="Program.cs" company="Delphi Software LTD">
// Copyright (c) Delphi Software LTD 2019. All rights reserved.
// </copyright>

using System;
using System.ServiceModel;
using System.Threading;
using GrabberDriverContracts;

namespace Grabber64bitService
{
    /// <summary>
    /// The entry point class of the grabber service.
    /// </summary>
    internal class Program
    {
        private static void Main(string[] args)
        {
            ManualResetEvent closeMainProcess = new ManualResetEvent(false);

            Uri baseAddresses = new Uri("http://localhost/Grabber64bitService");
            string endpointAddresses = "net.pipe://localhost/Grabber64bitService/GrabberDriver";

            GrabberDriver grabberDriver = new GrabberDriver(closeMainProcess);

            ServiceHost serviceHost = new ServiceHost(grabberDriver, baseAddresses);

            serviceHost.AddServiceEndpoint(typeof(IGrabberDriver), new NetNamedPipeBinding(NetNamedPipeSecurityMode.None), endpointAddresses);
            serviceHost.Open();

            Console.WriteLine("ServiceHost connection has been esstablished.");

            closeMainProcess.WaitOne();

            serviceHost.Close();
        }
    }
}
